package org.tet.applock.locker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.util.Log;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.util.PrefUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class MobileDataReceiver extends BroadcastReceiver {

    private static int process = 0;
    private static boolean isEnabling = false;
    private static boolean isUnlocked = false;
    // hold status since there can be 1 or more identical status events (device specific)
    private static boolean status = false;
    private String enableStr = "dataEnabled";
    private String disableStr = "dataDisabled";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("mobile data", "INTENT RECEIVED: " + action);

        if (action.equals(LockService.USER_PRESSED_BACK)) {
            isUnlocked = false;
            process = 0;
        }

        SharedPreferences sp = PrefUtils.appsPrefs(context);
        boolean isMobileDataLocked = sp.getBoolean("com.mobile.network", false);

        if (isMobileDataLocked && AppLockService.isRunning(context)) {

            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                int networkType = intent.getExtras().getInt(ConnectivityManager.EXTRA_NETWORK_TYPE);
                String mReason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
                boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                boolean isMobile = networkType == ConnectivityManager.TYPE_MOBILE;

//                if (!enableStr.equals(mReason) && !disableStr.equals(mReason))
//                    return;
                Log.d("mobile data", "is mobile " + isMobile + " reason => " + mReason + " no connectivity " + noConnectivity);
                if (isMobile) {

                    status = ! noConnectivity;//isMobileDataEnabled(context);

                    Log.d("mobile data", "reason => " + mReason + " |mobile data status => " + (status ? "enabled" : "disabled"));

                    if (process == 0) {
                        process = 1;
                        setMobileDataEnabled(context, !status);
                        isEnabling = status;
                        Log.d("mobile data", "password?");
                        LockService.showCompare(context, "Mobile Data", LockService.SWITCH_UNLOCK, R.drawable.mobile_data);
                    }
                    if (isUnlocked) {
                        process = 0;
                        isUnlocked = false;
                    }

                    Log.d("mobile data", "process value => " + process);
                }

            } else if (intent.getAction().equals(LockService.SWITCH_UNLOCK) && process != 0) {
                setMobileDataEnabled(context, isEnabling);
                isUnlocked = true;
            }
        }
    }

    private boolean isMobileDataEnabled(Context context) {
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);
        } catch (Exception e) {

        }
        return mobileDataEnabled;
    }

    private void setMobileDataEnabled(Context context, boolean enabled) {
        try {
            final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final Class conmanClass = Class.forName(conman.getClass().getName());
            final Field connectivityManagerField = conmanClass.getDeclaredField("mService");
            connectivityManagerField.setAccessible(true);
            final Object connectivityManager = connectivityManagerField.get(conman);
            final Class connectivityManagerClass = Class.forName(connectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);

            setMobileDataEnabledMethod.invoke(connectivityManager, enabled);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
