package org.tet.applock.locker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.tet.applock.R;
import org.tet.applock.locker.lock.AppLockService;
import org.tet.applock.locker.lock.LockService;
import org.tet.applock.locker.util.PrefUtils;
import org.tet.applock.locker.util.ScreenRotationUtil;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class ScreenRotationReceiver extends BroadcastReceiver {

    private static int process = 0;
    private static boolean isEnabling = false;
    private static boolean isUnlocked = false;

    private static boolean status = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("", "INTENT RECEIVED: " + action);

        if (action.equals(LockService.USER_PRESSED_BACK)) {
            isUnlocked = false;
            process = 0;
        }

        SharedPreferences sp = PrefUtils.appsPrefs(context);
        boolean isScreenRotationLocked = sp.getBoolean("com.screen.rotation", false);

        if (isScreenRotationLocked && AppLockService.isRunning(context)) {
            if (action.equals(ScreenRotationUtil.ACTION_SCREEN_ROTATION)) {
                status = ScreenRotationUtil.isRotationEnabled(context);

                if (process == 0) {
                    process = 1;
                    ScreenRotationUtil.enableRotation(context, !status);
                    isEnabling = status;
                    LockService.showCompare(context, "Screen Rotation", LockService.SWITCH_UNLOCK, R.drawable.screen_rotation);
                }
                if (isUnlocked) {
                    process = 0;
                    isUnlocked = false;
                }
            } else if (intent.getAction().equals(LockService.SWITCH_UNLOCK) && process != 0 && !isUnlocked) {
                ScreenRotationUtil.enableRotation(context, isEnabling);
                isUnlocked = true;
            }

        }
    }
}
