package org.tet.applock.locker.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by MustafaSarper on 23.5.2015.
 */
public class USBConnectionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("", "INTENT RECEIVED: " + action);
        if (action.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED))
            Toast.makeText(context, "USB Connection Detected!", Toast.LENGTH_SHORT);
    }
}
