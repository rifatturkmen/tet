package org.tet.applock.locker.appselect;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by MustafaSarper on 27.5.2015.
 */
public class CategoryViewHolder {
    public ImageView lock;
    public TextView text;
}
