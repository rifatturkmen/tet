package org.tet.applock.locker.util;

import android.content.ContentResolver;

/**
 * Created by MustafaSarper on 22.5.2015.
 */
public class AutoSyncUtil {

    public static String AUTO_SYNC_CHANGE = "org.tet.applock.AUTO_SYNC_CHANGE";
    public static boolean status = false;

    static {
        status = isEnabled();
    }

    public static boolean isEnabled() {
        boolean isEnabled = false;
        try {
            isEnabled = ContentResolver.getMasterSyncAutomatically();
        } catch (Exception exception) {
        }

        return isEnabled;
    }

    public static void setEnabled(boolean isEnabled) {
        try {
            ContentResolver.setMasterSyncAutomatically(isEnabled);
        } catch (Exception exception) {
        }
    }
}
