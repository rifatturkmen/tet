package org.tet.applock.locker.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.tet.applock.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cihad on 19.5.2015.
 */
public class PageViewActivity extends Fragment {

    MyPageAdapter pageAdapter;
    private List<Fragment> fragments;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.description_page, container, false);
        fragments = getFragments();
        pageAdapter = new MyPageAdapter(getChildFragmentManager(), fragments);
        ViewPager pager = (ViewPager) view.findViewById(R.id.viewpager);

        pager.setAdapter(pageAdapter);
        return view;
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();


        // TODO create fragments
        fList.add(MyFragment.newInstance("Fragment 1"));
        fList.add(MyFragment.newInstance("Fragment 2"));
        fList.add(MyFragment.newInstance("Fragment 3"));

        return fList;
    }
}
